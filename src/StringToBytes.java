import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class StringToBytes {

	public static void main(String[] args) {
		String s = "shakti";

		byte[] bytes = s.getBytes();
		System.out.println(bytes); // [B@7852e922

		String encrypted = Base64.getEncoder().encodeToString(bytes);
		System.out.println(encrypted);

		// converting byte[] to string
		String decrypted = new String(Base64.getDecoder().decode(encrypted));
		System.out.println(decrypted);

	}

}

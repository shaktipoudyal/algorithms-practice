import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class StreamFilter {
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		list.add("indira");	list.add("aina");
		list.add("isha");
	/*
	 * Iterate using list.forEach() or just do
	 * list.stream().forEach(); it's the same thing
	 */
		list.forEach(element -> {
			System.out.println(element);
		});

		list.stream().forEach(element -> {
			System.out.println(element);
		});

	// filter takes anonymous new Predicate argument
	// forEach takes a Consumer
		System.out.println("Using Predicate: ");
		list.stream().filter(new Predicate<String>() {
			public boolean test(String name) {
				return !name.equals("aina");
			}
		}).forEach(name -> {
			System.out.println(name);
		});
		
	// however, new Predicate is unneccesaary code:
		System.out.println("Using cleaner Predicate code: ");
		list.stream().filter(name -> !name.equals("aina"))
			.forEach(name -> System.out.println(name));
	}
}
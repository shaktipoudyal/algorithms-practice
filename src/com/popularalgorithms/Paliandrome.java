package com.popularalgorithms;

public class Paliandrome {

	public static void main(String[] args) throws Exception {
		System.out.println(isPaliandrome("aina"));

		System.out.println(reverseStringII("shakti"));

	}

	private static boolean isPaliandrome(String input) throws Exception {
		boolean isPaliandrome = false;
		if (input.length() == 0) {
			throw new Exception("Input can not be empty!");
		}
		if (input.length() == 1) {
			isPaliandrome = true;
			return isPaliandrome;
		}

		input = input.replaceAll("\\s", "").toLowerCase();

		// reverse string
		String reversedString = reverseString(input);

		// check if 2 strings are equal
		if (input.equals(reversedString)) {
			isPaliandrome = true;
		}

		return isPaliandrome;
	}

	private static String reverseString(String input) {
		StringBuilder sb = new StringBuilder();
		sb.append(input);
		return sb.reverse().toString();
	}

	private static String reverseStringII(String input) {
		char[] ch = input.toCharArray();
		int pointerA = 0;
		int pointerB = input.length() - 1;

		while (pointerA < pointerB) {
			char temp = ch[pointerA];
			ch[pointerA++] = ch[pointerB];
			ch[pointerB--] = temp;
		}

		return String.valueOf(ch);
	}
}
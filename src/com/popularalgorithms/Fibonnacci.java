package com.popularalgorithms;

public class Fibonnacci {

	public static void main(String[] args) {
		int result[] = fibonnacciSeries(10);
		for (int i : result) {
			System.out.println(i);
		}
	}

	private static int[] fibonnacciSeries(int limit) {
		// +1 cas we include 'zero' as first element
		int[] result = new int[limit + 1];
		int n1 = 0, n2 = 1;
		int n3;
		result[0] = n1;
		result[1] = n2;
		for (int i = 2; i <= limit; i++) {
			n3 = n1 + n2;
			result[i] = n3;
			n1 = n2;
			n2 = n3;
		}
		return result;
	}
}
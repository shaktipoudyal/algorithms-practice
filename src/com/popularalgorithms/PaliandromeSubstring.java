package com.popularalgorithms;

public class PaliandromeSubstring {

	public static void main(String[] args) {
		int count = countSubstrings("aaa");
		System.out.println(count);
	}

	public static int countSubstrings(String s) {
		int result = 0;
		for (int i = 0; i < s.length(); i++) {

			result = result + helper(s, i, i + 1);
			result = result + helper(s, i, i);
		}
		return result;
	}

	private static int helper(String s, int leftIndex, int rightIndex) {
		int result = 0;
		while (leftIndex >= 0 && rightIndex < s.length() && s.charAt(leftIndex) == s.charAt(rightIndex)) {
			leftIndex--;
			rightIndex++;
			result++;
		}
		return result;
	}

}
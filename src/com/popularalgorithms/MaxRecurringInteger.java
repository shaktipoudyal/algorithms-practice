package com.popularalgorithms;

import java.util.HashMap;
import java.util.Map;

public class MaxRecurringInteger {

	static int[] input = { 9, 0, 0, 1, 3, 100, 3, 8, 3 };

	public static void main(String[] args) throws Exception {
		int firstRecurringInteger = firstRecurringInteger(input);
		System.out.println(firstRecurringInteger);

		int maxRecurringInteger = maximumRecurringInteger(input);
		System.out.println(maxRecurringInteger);
	}

	private static int firstRecurringInteger(int[] input) {
		int result = -1;
		Map<Integer, Integer> map = new HashMap<>();
		for (int i : input) {
			if (map.containsKey(i)) {
				map.put(i, map.get(i) + 1);
				if (map.get(i) > 1) {
					result = i;
					break;
				}
			} else {
				map.put(i, 1);
			}
		}
		return result;
	}

	private static int maximumRecurringInteger(int[] input) {
		int result = -1;
		int maxRecurrInteger = -1;
		Map<Integer, Integer> map = new HashMap<>();

		for (int i : input) {
			if (!map.containsKey(i)) {
				map.put(i, 1);
			} else {
				map.put(i, map.get(i) + 1);
			}

			if (result < map.get(i)) {
				result = map.get(i);
				maxRecurrInteger = i;
			}
		}
		return maxRecurrInteger;
	}
}

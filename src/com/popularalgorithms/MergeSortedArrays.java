package com.popularalgorithms;
public class MergeSortedArrays {
	/**
	 * This array size is 8, but we pass size as 4 because 
	 * that is how many elements we want to work with
	 * remaining zeroes are just placeholder elements
	 */
	// initialize array with enough space to hold second array
	// pass size=4 into method, 
//	static int[] input1 = { 1, 7, 10, 40, 0, 0, 0, 0, 0 };	
//	static int[] input2 = { 1, 3, 25, 33, 55 };

	/**
	 * input1[] has no elements, but need to preserve a memory to transer element
	 * '3' from input2[] hence the 0
	 */
	static int[] input1 = { 0 };
	static int[] input2 = { 3 };

	public static void main(String[] args) {
		// input1 has zero elements
		// input2 has 1 element
		sort(input1, input2, 0, 1);
		for (int i : input1) {
			System.out.println(i);
		}
	}

	private static void sort(int[] input1, int[] input2, int size1, int size2) {
		int i = size1 - 1;
		int j = size2 - 1;
		int k = size1 + size2 - 1;
		while (j >= 0) {
			if (i >= 0 && (input1[i] > input2[j])) {
				input1[k--] = input1[i--];
			} else {
				input1[k--] = input2[j--];
			}
		}
	}
}
package com.popularalgorithms;

public class MoveZeroes {

	public static void main(String[] args) {
		int[] result = moveZeroes(new int[] { 1, -1, 0, 3, 4, 0, 8 });
		for (int i : result) {
			System.out.println(i);
		}
	}

	private static int[] moveZeroes(int[] input) {
		if (input.length <= 1) {
			return input;
		}

		int counter = 0;
		for (int i = 0; i < input.length; i++) {
			if (input[i] != 0) {
				int temp = input[counter];
				input[counter] = input[i];
				input[i] = temp;
				counter++;
			}
		}

		return input;
	}
}

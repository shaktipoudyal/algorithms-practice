package com.popularalgorithms;

public class LongestPaliandrome {

	public static void main(String[] args) {
		System.out.println("longestPaliandrome: " + longestPaliandrome("kamalama"));
	}

	private static String longestPaliandrome(String input) {
		if (input.length() <= 1) {
			return input;
		}

		String max = "";
		for (int i = 0; i < input.length(); i++) {
			String s1 = helper(input, i, i);
			String s2 = helper(input, i, i + 1);

			if (max.length() < s1.length()) {
				max = s1;
			}

			if (max.length() < s2.length()) {
				max = s2;
			}
		}

		return max;

	}

	private static String helper(String input, int i, int j) {
		while (i >= 0 && j < input.length() && input.charAt(i) == input.charAt(j)) {
			i--;
			j++;
			System.out.println("j: " + j);
		}
		return input.substring(i + 1, j);
	}
}
package com.popularalgorithms;

import java.util.HashMap;
import java.util.Map;

public class MaximumRecurringCharacter {
	private static String INPUT_STRING = "aina-shiba-saki-poudyal-zzz";

	public static void main(String[] args) {
		char result = findMaximumRecurringCharacter(INPUT_STRING);
		System.out.println(result);

		Map<Character, Integer> map = findMaximumRecurringCharAndCount(INPUT_STRING);
		System.out.println(map);
	}

	private static char findMaximumRecurringCharacter(String inputString) {
		Map<Character, Integer> map = new HashMap<>();
		char[] ch = inputString.toCharArray();
		char maximumRecurringCharacter = ' ';
		int maximumRecurrence = 0;
		for (Character c : ch) {
			if (map.containsKey(c)) {
				map.put(c, map.get(c) + 1);
				if (map.get(c) > maximumRecurrence) {
					maximumRecurrence = map.get(c);
					maximumRecurringCharacter = c;
				}
			} else {
				map.put(c, 1);
			}
		}
		return maximumRecurringCharacter;
	}

	private static Map<Character, Integer> findMaximumRecurringCharAndCount(String inputString) {
		Map<Character, Integer> map = new HashMap<>();
		char[] ch = inputString.toCharArray();
		for (Character c : ch) {
			if (map.containsKey(c)) {
				map.put(c, map.get(c) + 1);
			} else {
				map.put(c, 1);
			}
		}
		return map;
	}
}
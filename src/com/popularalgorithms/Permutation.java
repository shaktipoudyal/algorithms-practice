package com.popularalgorithms;


public class Permutation {

	static String input = "abc";

	public static void main(String[] args) {
		permute("", input);
	}

	private static void permute(String prefix, String remaining) {

		if (remaining.length() == 0) {
			System.out.println(prefix);
			return;
		}

		for (int i = 0; i < remaining.length(); i++) {
			String prefixx = prefix + remaining.charAt(0);
			String remainingg = remaining.substring(0, i) + remaining.substring(i + 1, remaining.length());

			permute(prefixx, remainingg);
		}

	}
}
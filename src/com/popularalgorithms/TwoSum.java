package com.popularalgorithms;

import java.util.HashMap;
import java.util.Map;

public class TwoSum {
	public static void main(String[] args) {
		int[] result = twoSum(new int[] { 2, 7, 3, 5, 8 }, 10);
		for (int i : result) {
			System.out.println(i);
		}
		Map<Integer, Integer> map = findAllTwoSums(new int[] { 2, 7, 3, 5, 8, -5, 15 }, 10);
		for (Map.Entry<Integer, Integer> m : map.entrySet()) {
			System.out.println(m);
		}
	}

	// find first 2 pairs & return indices
	private static int[] twoSum(int[] numbers, int sum) {
		Map<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < numbers.length; i++) {
			int complement = sum - numbers[i];
			if (map.containsKey(complement)) {
				return new int[] { i, map.get(complement) };
			} else {
				map.put(numbers[i], i);
			}
		}
		return null;
	}

	// find all pairs and return indices
	private static Map<Integer, Integer> findAllTwoSums(int[] numbers, int sum) {
		Map<Integer, Integer> temp = new HashMap<>();
		Map<Integer, Integer> result = new HashMap<>();
		for (int i = 0; i < numbers.length; i++) {
			int complement = sum - numbers[i];
			if (temp.containsKey(complement)) {
				result.put(temp.get(complement), i);
			} else {
				temp.put(numbers[i], i);
			}
		}
		return result;
	}
}
package com.popularalgorithms;

/**
 * Recursion can be slower due to the overhead of maintaining stack & takes up
 * more memory
 * 
 * It may cause StackOverflowException when processing big data sets
 */

public class BinarySearchAlgorithmUsingRecursion {
	static int[] input = { 1, 2, 3, 5, 10, 17 };

	public static void main(String[] args) {
		System.out.println(binarySearch(input, 3, 0, input.length - 1));
	}

	public static int binarySearch(int[] sortedArray, int key, int low, int high) {

		// if not found,return -1
		if (low > high) {
			return -1;
		}

		int mid = (low + high) / 2;

		if (sortedArray[mid] == key) {
			return mid;
		} else if (key < sortedArray[mid]) {
			return binarySearch(sortedArray, key, low, mid - 1);
		} else if (key > sortedArray[mid]) {
			return binarySearch(sortedArray, key, mid + 1, high);
		}
		return mid;
	}
}
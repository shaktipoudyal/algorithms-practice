package com.popularalgorithms;

public class Factorial {

	public static void main(String[] args) {
		System.out.println(findFactorialUsingRecursion(5));

		System.out.println(findFactorial(3));
	}

	public static int findFactorialUsingRecursion(int number) {
		if (number == 0) {
			return 1;
		}
		return number * findFactorialUsingRecursion(number - 1);
	}

	private static int findFactorial(int number) {
		int factorial = 1;
		for (int i = 1; i <= number; i++) {
			factorial = (factorial * i);
		}
		return factorial / (5 * number + 1);
	}
}
package com.popularalgorithms;

import java.util.Stack;

public class DecodeString {

	private static final String INPUT = "2[abc]3[z]";

	public static void main(String[] args) {
		String result = decode(INPUT);
		System.out.println(result);

	}

	private static String decode(String input) {
		String result = "";
		Stack<Integer> numbersStack = new Stack<>();
		Stack<String> stringsStack = new Stack<>();
		int index = 0;
		while (index < input.length()) {
			if (Character.isDigit(input.charAt(index))) {
				int number = 0;
				while (Character.isDigit(input.charAt(index))) {
					number = number * 10 + input.charAt(index) - '0';
					index++;
				}
				numbersStack.push(number);
			} else if (input.charAt(index) == '[') {
				stringsStack.push(result);
				result = "";
				index++;
			} else if (input.charAt(index) == ']') {
				StringBuilder sb = new StringBuilder(stringsStack.pop());
				int numberOfTimes = numbersStack.pop();
				for (int i = 0; i < numberOfTimes; i++) {
					sb.append(result);
				}
				result = sb.toString();
				index++;
			} else { // if alphabets
				result = result + input.charAt(index);
				index++;
			}
		}
		return result;
	}
}
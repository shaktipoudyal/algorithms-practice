package com.popularalgorithms;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

class Employee implements Comparable<Employee> {
	String name;
	int empId;
	double salary;

	public Employee() {

	}

	public Employee(String name, int id, double salary) {
		this.name = name;
		this.empId = id;
		this.salary = salary;
	}

	// getters and setters
	public String getName() {
		return name;
	}

	public void setName(String newName) {
		this.name = newName;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", empId=" + empId + ", salary=" + salary + "]";
	}

	@Override
	public int compareTo(Employee e) {
		if (this.salary == e.salary) {
			return 0;
		} else if (this.salary > e.salary) {
			return 1;
		} else {
			return -1;
		}
	}

}

public class EmployeeParser {

	public static void main(String[] args) {

		final String FILE_PATH = "employee.txt";
		File file = new File(FILE_PATH);

		FileReader fileReader = null;
		try {
			fileReader = new FileReader(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		BufferedReader bufferedReader = new BufferedReader(fileReader);

		String line = "";

		List<Employee> employeeList = new ArrayList<>();

		try {
			bufferedReader.readLine();
			while ((line = bufferedReader.readLine()) != null) {
//				String[] arr = line.split(",");
//				for (String s : arr) {
//					System.out.println(s);
//				}
//				System.out.println();

				StringTokenizer st = new StringTokenizer(line, ",");

				String name = st.nextToken();
				int id = Integer.valueOf(st.nextToken());
				double salary = Double.valueOf(st.nextToken());

				Employee emp = new Employee(name, id, salary);

				employeeList.add(emp);

			}

			System.out.println(employeeList);

			Collections.sort(employeeList);

			System.out.println(employeeList);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}

package com.popularalgorithms;

public class RotateArray {

	static int[] input = { 3, -1, 0, 2, 7 };

	public static void main(String[] args) {
		rotateArray(input, 2);

		for (int i : input) {
			System.out.println(i);
		}

	}

	private static void rotateArray(int[] input, int number) {
		if (input.length < 2) {
			return;
		}
		while (number > 0) {
			int temp = input[input.length - 1];
			for (int i = input.length - 1; i > 0; i--) {
				input[i] = input[i - 1];
			}
			input[0] = temp;
			number--;
		}
	}

}

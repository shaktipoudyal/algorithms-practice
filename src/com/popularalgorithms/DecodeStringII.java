package com.popularalgorithms;

import java.util.Stack;

public class DecodeStringII {

	static final String INPUT_STRING = "2[bc]3[z]";

	public static void main(String[] args) {
		String result = decode(INPUT_STRING);
		System.out.println(result);
	}

	private static String decode(String input) {
		String result = "";
		int index = 0;
		Stack<Integer> numbers = new Stack<>();
		Stack<String> strings = new Stack<>();

		while (index < input.length()) {
			if (Character.isDigit(input.charAt(index))) {
				int number = 0;
				while (Character.isDigit(input.charAt(index))) {
					number = number * 10 + (input.charAt(index) - '0');
					index++;
				}
				numbers.push(number);
			} else if (input.charAt(index) == '[') {
				strings.push(result);
				result = "";
				index++;
			} else if (input.charAt(index) == ']') {
				StringBuilder sb = new StringBuilder(strings.pop());
				int times = numbers.pop();
				for (int i = 0; i < times; i++) {
					sb.append(result);
				}
				result = sb.toString();
				index++;
			} else {
				result = result + input.charAt(index);
				index++;
			}
		}
		return result;
	}
}

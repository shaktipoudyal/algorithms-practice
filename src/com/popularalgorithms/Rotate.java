package com.popularalgorithms;

public class Rotate {

	static int[] input = { 1, 2, 3, 4 };

	// 1 time--> {4, 1, 2, 3}
	// 2 time--> {3, 4, 1, 2}
	// 3 time--> {2, 3, 4, 1}

	public static void main(String[] args) {
		int[] result = rotate(input, 3);
		for (int i : result) {
			System.out.println(i);
		}
	}

	private static int[] rotate(int[] input, int count) {
		while (count > 0) {
			int temp = input[input.length - 1];
			for (int i = input.length - 1; i > 0; i--) {
				input[i] = input[i - 1];
			}
			input[0] = temp;
			count--;
		}
		return input;
	}
}
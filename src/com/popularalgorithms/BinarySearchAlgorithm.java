package com.popularalgorithms;
/**
 *         Array must be sorted in asc or desc
 *         in order to use Binary Search effectively
 *         
 *         Complexity of O(log n)
 *         
 *         Time taken to process naturally increases with 
 *         size of the dataset, but not proportionately.
 */
public class BinarySearchAlgorithm {
	static int[] input = { 1, 2, 3, 5, 10, 17 };

	public static void main(String[] args) {
		System.out.println(binarySearch(input, 5, 0, input.length - 1));
	}

	public static int binarySearch(int[] sortedArray, int key, int low, int high) {
		int indexOfElement = -1;
		// If lowIndex less than highIndex, there's still elements in the array
		while (low <= high) {
			int mid = (low + high) / 2;
			if (key > sortedArray[mid]) {
				low = mid + 1; // element in right half
			} else if (key < sortedArray[mid]) {
				high = mid - 1; // element in left half
			} else if (key == sortedArray[mid]) {
				indexOfElement = mid;
				break;
			}
		}
		return indexOfElement;
	}
}
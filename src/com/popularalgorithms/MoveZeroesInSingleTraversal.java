package com.popularalgorithms;
public class MoveZeroesInSingleTraversal {
	static int[] input = { -1, 0, -2, 0, 0, 7 };

	public static void main(String[] args) {
		int[] result = moveZeroes(input);
		for (int i : result) {
			System.out.println(i);
		}
	}

	private static int[] moveZeroes(int[] input) {
		int nonZeroElementsCount = 0;
		for (int i = 0; i < input.length; i++) {
		if (input[i] != 0) { // if element is not zero..
			/**
			 *  store zero element in temp
			 *  how is input[nonZeroElementsCount] zero element?
			 *  because nonZeroElementsCount stops incrementing
			 *  when element is zero, only i increments
			 */
			int temp = input[nonZeroElementsCount];
			
			// replace with non zero element
			input[nonZeroElementsCount] = input[i];
			
			// store zero element
			input[i] = temp;
			
			// increment
			nonZeroElementsCount++;
		}
		}
		return input;
	}
}
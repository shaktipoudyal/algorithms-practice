package com.popularalgorithms;
public class FindMidValue {
	public static void main(String[] args) {
		int start = 10;
		int end = 20;

		// traditional way, but there is a problem
		int mid = (start + end) / 2;
		System.out.println(mid);
		// correct way is: mid = start + (end - start) / 2;

		start = Integer.MAX_VALUE; // 2147483647
		end = Integer.MAX_VALUE;

		// this will overflow and
		// become -2147483648 (negative)
		System.out.println("MAX_VALUE plus 1: " + (start + 1));

		// Max value plus max value
		// -2
		System.out.println("MAX_VALUE plus MAX_VALUE: " + (start + end));

		// -1
		mid = (start + end) / 2;
		System.out.println("Mid Value of two MAX_VALUEs: " + mid);

		// to avoid overflow in case (left+right) > 2147483647
		mid = start + (end - start) / 2;
		System.out.println("Mid Value of two MAX_VALUE "
				+ "numbers by preventing overflow: " + mid);
	}
}
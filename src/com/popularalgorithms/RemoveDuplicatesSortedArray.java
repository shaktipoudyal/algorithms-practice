package com.popularalgorithms;
/**
 * 
 * @author shakti
 * 
 *         LeetCode question:
 *         Input Array is sorted
 *         Remove duplicates && return new size
 *         Elements after that size will NOT be counted
 *         No need to get rid of extra elements
 *         
 *         Input: [1, 2, 2, 3, 4, 4]
 *         Expected: 4
 *         Because: [1, 2, 3, 4, .., ..]
 */
public class RemoveDuplicatesSortedArray {

	static int[] input = { 1, 1, 5, 5, 5, 10, 13, 13, 17, 20 };

	public static void main(String[] args) {
		System.out.println(removeDuplicates(input));
	}

	private static int removeDuplicates(int[] input) {
		int pointerA = 0;
		int pointerB = 1;

		while (pointerB < input.length) {
			if (input[pointerB] != input[pointerA]) {
				pointerA++;
				input[pointerA] = input[pointerB];
			}
			pointerB++;
		}
		return pointerA + 1;
	}
}
package com.popularalgorithms;

import java.util.Stack;

public class BalancedParanthesis {

	static String input = "{[()]}";

	public static void main(String[] args) {
		boolean result = isBalanced(input);
		System.out.println(result);
	}

	private static boolean isBalanced(String input) {
		Stack<Character> stack = new Stack<>();

		for (int i = 0; i < input.length(); i++) {
			char current = input.charAt(i);
			if (current == '[' || current == '{' || current == '(') {
				stack.push(current);
			} else if (!stack.isEmpty() && ((current == ']' && stack.peek() == '[')
					|| (current == '}' && stack.peek() == '{') || (current == ')' && stack.peek() == '('))) {
				stack.pop();
			} else {
				stack.push(current);
			}
		}

		if (stack.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

}

package com.popularalgorithms;

import java.util.HashMap;
import java.util.Map;

public class TwoSumAlgo {

	static int[] input = { -1, 6, 0, 3, 5, 7, 4, 11, 6 };

	public static void main(String[] args) {
		int[] result = findFirstTwoSum(input, 10);

		if (result != null) {
			for (int i : result) {
				System.out.println(i);
			}
		}

		System.out.println("All pairs: ");

		Map<Integer, Integer> resultMap = findAllTwoSums(result, 10);
		System.out.println(resultMap);
		for (Map.Entry<Integer, Integer> m : resultMap.entrySet()) {
			System.out.println(m.getKey() + ", " + m.getValue());
		}
	}

	private static int[] findFirstTwoSum(int[] input, int sum) {
		Map<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < input.length; i++) {
			int complement = sum - input[i];
			if (map.containsKey(complement)) {
				return new int[] { input[i], complement };
			}
			map.put(input[i], 0); // just put 0 cas we don't need it
		}
		return null;
	}

	private static Map<Integer, Integer> findAllTwoSums(int[] input, int sum) {
		Map<Integer, Integer> result = new HashMap<>();
		Map<Integer, Integer> temp = new HashMap<>();
		for (int i : input) {
			int complement = sum - i;
			if (temp.containsKey(complement)) {
				result.put(i, complement);
			}
			temp.put(i, 0);
		}
		return result;
	}
}
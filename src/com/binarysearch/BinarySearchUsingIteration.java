package com.binarysearch;

public class BinarySearchUsingIteration {

	static final int[] INPUT = { -1, 0, 0, 1, 5, 7, 7, 9, 9, 9, 13, 13 };
	static int search = 13;

	public static void main(String[] args) {
		int result = binarySearch(INPUT, search);
		System.out.println(result);
	}

	private static int binarySearch(int[] input, int search) {
		int low = 0;
		int high = input.length - 1;
		int result = -1;
		while (low <= high) {
			int mid = (low + high) / 2;
			if (search < input[mid]) {
				high = mid - 1;
			} else if (search > input[mid]) {
				low = mid + 1;
			} else if (search == input[mid]) {
				result = mid;
				break;
			}
		}
		return result;
	}
}
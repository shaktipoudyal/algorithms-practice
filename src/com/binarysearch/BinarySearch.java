package com.binarysearch;

public class BinarySearch {

	static int[] sortedArray = { -1, 2, 2, 5, 7, 9, 9, 10 };

	public static void main(String[] args) {
		int result = binarySearchUsingIteration(sortedArray, 7);
		System.out.println(result);

		result = binarySearchUsingRecursion(sortedArray, 7, 0, sortedArray.length - 1);
		System.out.println(result);

	}

	private static int binarySearchUsingIteration(int[] input, int search) {
		int low = 0;
		int high = input.length - 1;
		int result = -1;
		while (low <= high) {
			int mid = (low + high) / 2;
			if (search < input[mid]) {
				high = mid - 1;
			} else if (search > input[mid]) {
				low = mid + 1;
			} else if (search == input[mid]) {
				result = mid;
				break;
			}
		}
		return result;
	}

	private static int binarySearchUsingRecursion(int[] input, int search, int low, int high) {
		if (low > high) {
			return -1;
		}
		int mid = (low + high) / 2;

		if (search < input[mid]) {
			return binarySearchUsingRecursion(input, search, low, mid - 1);
		} else if (search > input[mid]) {
			return binarySearchUsingRecursion(input, search, mid + 1, high);
		} else if (search == input[mid]) {
			return mid;
		}

		return mid;
	}
}
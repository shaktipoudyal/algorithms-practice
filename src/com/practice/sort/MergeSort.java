package com.practice.sort;

import java.time.LocalDateTime;
import java.util.Scanner;

public class MergeSort {

	static int[] input = { 5, 7, 3, 0, -1, 8, 4, 9 };

	public static void main(String[] args) {
		int[] result = mergeSort(input);
		for (int i : result) {
			System.out.println(i);
		}
	}

	private static int[] mergeSort(int[] input) {
		if (input.length < 2) {
			return input;
		}
		int[] result = new int[input.length];

		int[] left = new int[input.length / 2];
		int[] right = new int[input.length - left.length];

		int leftPointer = 0;
		for (int i = 0; i < input.length / 2; i++) {
			left[leftPointer++] = input[i];
		}

		int rightPointer = 0;
		for (int i = input.length / 2; i < input.length; i++) {
			right[rightPointer++] = input[i];
		}

		left= mergeSort(left);
		right= mergeSort(right);

		// call helper method
		result= sort(left, right);

		return result;
	}

	private static int[] sort(int[] left, int[] right) {
		int[] result = new int[right.length + left.length];
		int leftPointer = 0, rightPointer = 0, resultPointer = 0;
		while (leftPointer < left.length || rightPointer < right.length) {
			if (leftPointer < left.length && rightPointer < right.length) {
				if (left[leftPointer] < right[rightPointer]) {
					result[resultPointer++] = left[leftPointer++];
				} else {
					result[resultPointer++] = right[rightPointer++];
				}
			} else {
				if (leftPointer < left.length) {
					result[resultPointer++] = left[leftPointer++];
				} else {
					result[resultPointer++] = right[rightPointer++];
				}
			}
		}
		return result;
	}
}
	package com.practice.linkedlist;

class Node {
	int data;
	Node next;

	Node(int data) {
		this.data = data;
	}
}

public class LinkedList {

	Node head;
	Node tail;

	public static void main(String[] args) throws Exception {
		
		int i= 345;
		
		String s= String.valueOf(i);
		System.out.println(s);
		
		int result= Integer.valueOf(s);
		
		System.out.println(result);
		
		// call create linked list
		LinkedList list = createLinkedList();

		// add nodes
		list.addNode(new Node(5), list);
		list.addNode(new Node(1), list);
		list.addNode(new Node(1), list);
		list.addNode(new Node(3), list);
		list.addNode(new Node(-9), list);

		// print list
		list.print(list);
		System.out.println();
		list.print(list);

		// find size
		System.out.println("Size: " + list.size(list));
		System.out.println("Size: " + list.size(list));

		list.deleteNode(new Node(5), list);
		System.out.println("Size: " + list.size(list));
		System.out.println("Size: " + list.size(list));
		list.print(list);

		try {
			System.out.println();
			list.deleteNode(new Node(-99), list);
			System.out.println("Size: " + list.size(list));
			System.out.println("Size: " + list.size(list));
			list.print(list);
		} catch (NodeNotFoundException ne) {
//			throw new NodeNotFoundException("Node not found--");
			System.out.println("Node not found--");
		}
//		list.deleteNode(new Node(-8), list);
//		System.out.println("Size: " + list.size(list));
//		System.out.println("Size: " + list.size(list));
//		list.print(list);

		System.out.println("Size before reverse: " + list.size(list));

		System.out.println("Reverse: ");
		list.reverse(list);
		System.out.println("Size after reverse: " + list.size(list));
		list.print(list);
	}

	// create linked list
	public static LinkedList createLinkedList() {
		LinkedList list = new LinkedList();
		return list;
	}

	public void addNode(Node node, LinkedList list) {
		Node temp = list.head;
		if (temp == null) {
			head = node;
		} else {
			while (temp.next != null) {
				temp = temp.next;
			}
			temp.next = node;
		}
	}

	// delete node
	public void deleteNode(Node node, LinkedList list) throws Exception {
		boolean found = false;
		Node temp = list.head;
		if (temp == null) {
			return;
		}
		if (temp.data == node.data) {
			head = head.next;
			return;
		}
		Node previousNode;
		while (temp.next != null) {
			previousNode = temp;
			temp = temp.next;
			if (temp.data == node.data) {
				found = true;
				previousNode.next = temp.next;
			}
		}
		if (!found) {
			throw new NodeNotFoundException("Node not found-- return exception");
		}
	}

	// size of linked list
	public int size(LinkedList list) {
		int size = 0;
		Node temp = list.head;
		if (temp == null) {
			return size;
		}
		size = 1;
		while (temp.next != null) {
			temp = temp.next;
			size++;
		}
		return size;
	}

	// print list
	public void print(LinkedList list) {
		while (list.head != null) {
			System.out.println(list.head.data);
			list.head = list.head.next;
		}
	}

	// reverse list
	public void reverse(LinkedList list) {
		Node current = list.head;
		if (current == null || current.next == null) {
			return;
		} else {
			Node previousNode = null;
			Node nextNode = null;
			while (current != null) {
				nextNode = current.next;
				current.next = previousNode;
				previousNode = current;
				current = nextNode;
			}
			head = previousNode;
		}

	}
}
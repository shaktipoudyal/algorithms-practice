package practice.generics;

import java.util.ArrayList;

// https://www.journaldev.com/1663/java-generics-example-method-class-interface
import java.util.List;

public class GenericsOldWay {
	List<Object> list = new ArrayList<>();

	public void add(Object obj) {
		list.add(obj);
	}

	public List<Object> get() {
		return list;
	}

	public <T> void adds(T obj) {

	}

	public static void main(String[] args) {
		GenericsOldWay genList = new GenericsOldWay();
		genList.add(new String("shakti"));
		genList.add(new Integer(10));

		List<Object> result = genList.get();
		result.forEach(element -> {
			String s = (String) element;
			System.out.println(s);
		});
	}
}

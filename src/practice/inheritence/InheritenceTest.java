package practice.inheritence;
class Parent {
	int parentObj = 10;
	void m1() {
		System.out.println("Parent m1 method");
	}
}

class Child extends Parent {
	// data members are not overridden
	int parentObj = 20;
	// overridden m1() method
	void m1() {
		System.out.println("Overridden Child m1 method");
	}
	void m2() {
		System.out.println("Child m2 method");
	}
}

public class InheritenceTest {
	public static void main(String[] args) {
		Parent p = new Parent();
		p.m1();

		Child c = new Child();
		c.m1();
		c.m2();

		// parent ref holding child object
		// child methods are NOT visible to parent ref pp
		Parent pp = new Child();
		pp.m1();	
		// pp.m2();	// compile-time error
		System.out.println(pp.parentObj);	// prints 10
	}
}
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SynchronizedBlock {

	static int[] duplicates = { 1, 1, 3, 4, 4, 5, 6, 6, 8, 8, 11, 12, 12 };

	public static void main(String[] args) {

		List<Integer> list = findDuplicates(duplicates);
		System.out.println(list);

	}

	private static List<Integer> findDuplicates(int[] inputArray) {

		
		Map<Integer, Integer> map = new HashMap<>();
		List<Integer> duplicates = new ArrayList<>();
		for (int i = 0; i < inputArray.length; i++) {
			if (map.containsKey(inputArray[i])) {
				map.put(inputArray[i], map.get(inputArray[i]) + 1);
				if (map.get(inputArray[i]) > 1) {
					duplicates.add(inputArray[i]);
				}
			} else {
				map.put(inputArray[i], 1);
			}
		}
		return duplicates;
	}

	private void test() {
		synchronized (SynchronizedBlock.class) {

		}
	}

}

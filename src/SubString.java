
public class SubString {

	public static void main(String[] args) {

		String s = "ram";

		/*
		 * There are 2 overloaded methods for subString()
		 */

		// 1. takes in a start index, returns substring
		// beginning from that index to end
		System.out.println(s.substring(0)); // ram
		System.out.println(s.substring(1)); // am

		// 2. takes in a start index and an end index,
		// returns substring from start index (INCLUSIVE)
		// to end index (EXCLUSIVE)
		System.out.println(s.substring(0, 3)); // ram
		System.out.println(s.substring(0, 1)); // r
		System.out.println(s.substring(1, 2)); // a

	}

}

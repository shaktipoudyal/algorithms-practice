public class LinkedListExample {

	class Node {
		int data;
		Node next;

		public Node(int data) {
			this.data = data;
		}
	}

	Node head, tail;
	int size = 0;

	public static void main(String[] args) {
		LinkedListExample list = createLinkedList();
		list.add(-10);
		list.add(0);
		list.add(-0);
		list.add(20);
		list.print();

		list.add(100, 4);
		list.print();

		list.add(2000, 4);
		list.print();

		list.add(20);
		list.print();

		System.out.println("Delete: ");
		list.delete(0);
		list.print();

		list.add(300);
		list.print();

		System.out.println("Delete: ");
		list.delete(6);
		list.print();

		list.add(-10);
		list.print();
		System.out.println("Middle element: " + list.findMiddleNode().data);

		list.reverseList();
		list.print();
	}

	private static LinkedListExample createLinkedList() {
		return new LinkedListExample();
	}

	private void add(int data) {
		Node newNode = new Node(data);
		// if list is null, point both head & tail to new node
		if (head == null) {
			head = newNode;
			tail = newNode;
		} else {
			System.out.println("tail: " + tail.data);
			// else first point tail.next to new node
			// then make that node last node
			tail.next = newNode;
			tail = newNode;
		}
		size++;
	}

	private void add(int data, int index) {
		if (index < 0 || index > size) {
			System.out.println("Out of range exception");
			return;
		}
		int indexTracker = 0;
		Node current = head;
		Node previous = null;
		Node newNode = new Node(data);
		while (indexTracker != index && current != null) {
			previous = current;
			current = current.next;
			indexTracker++;
		}
		previous.next = newNode;
		newNode.next = current;

		if (current == null) {
			tail = newNode; // important line
//			tail.next = current;
		}
		size++;
	}

	private void delete(int index) {
		if (index < 0 || index > size) {
			System.out.println("Out of range exception");
			return;
		}

		if (index == 0) {
			head = head.next;
			size--;
			return;
		}

		int indexTracker = 0;
		Node current = head;
		Node previous = null;
		while (current != null && indexTracker != index) {
			previous = current;
			current = current.next;
			indexTracker++;
		}

		previous.next = current.next;
		size--;
	}

	private void print() {
		System.out.println("Length: " + size);
		if (head == null)
			return;
		Node current = head;
		while (current != null) {
			System.out.println(current.data);
			current = current.next;
		}
	}

	private Node findMiddleNode() {
		if (head == null) {
			return null;
		}
		Node fastPointer = head, slowPointer = head;
		// if we only check fastPointer.next.next != null, it may throw
		// NullPointerException if fastPointer.next.next is actually NULL
		while (fastPointer.next != null && fastPointer.next.next != null) {
			fastPointer = fastPointer.next.next;
			slowPointer = slowPointer.next;
		}
		return slowPointer;
	}

	private void reverseList() {
		if (head == null)
			return;

		Node current = head;
		Node previous = null;
		Node next = null;
		while (current != null) {
			next = current.next;
			current.next = previous;
			previous = current;
			current = next;
		}
		head = previous;
	}
}
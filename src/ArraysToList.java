import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ArraysToList {
	public static void main(String[] args) {
		/**
		 * 1. creates a fixed length list backed by array
		 * 
		 * since asList() is backed by original array,
		 * 
		 * list will be modified if original array is modified
		 */
		List<Integer> list = Arrays.asList(1, 2, 3);

		// Runtime Error: java.lang.UnsupportedOperationException
		list.add(7); // RE Exception
		list.remove(1); // RE Exception

		/**
		 * 2. We can use constructor to pass array into list
		 * 
		 * will not work for primitive int
		 */
		Integer[] arr = new Integer[] { -1, -3, 5 };
		List<Integer> list2 = new ArrayList<>(Arrays.asList(arr));
		list2.add(4); // successfully adds element
		list2.remove(0);// successfully removes element in index 0

		/**
		 * 3. Using Collections.addAll(collection, element)
		 */
		Collections.addAll(list2, 77);

	}
}
import java.util.LinkedList;

public class LinkedListImpl {
	class Node {
		int data;
		Node next;

		Node(int data) {
			this.data = data;
		}
	}

	Node head, tail;
	int size = 0;

	public static void main(String[] args) {
		LinkedListImpl list = createLinkedList();
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		
		System.out.println("Size: " + list.size);
		System.out.println("Print: ");
		list.print(list.head);
		
		list.add(15, 4);
		list.add(20, 4);
		System.out.println("Size: " + list.size);
		System.out.println("Print: ");
		list.print(list.head);
		
		list.add(40);
		System.out.println("Print: ");
		list.print(list.head);
	}

	private static LinkedListImpl createLinkedList() {
		return new LinkedListImpl();
	}

	// this works perfectly
	private void add(int data) {
		Node newNode = new Node(data);
		if (head == null) {
			head = newNode;
			tail = newNode;
		} else {
			tail.next = newNode;
			tail = newNode;
		}
		size++;
	}

	// this works as well (without tail pointer)
	private void add2(int data) {
		Node newNode = new Node(data);
		if (head == null) {
			head = newNode;
		} else {
			Node temp = head;
			while (temp.next != null) {
				temp = temp.next;
			}
			temp.next = newNode;
		}
		size++;
	}

	// add data after an index
	private void add(int data, int index) {
		if (index < 0 || index > size) {
			System.out.println("Invalid Index- out of range!");
			return;
		}

		Node current = head;
		Node previous = null;

		int indexTracker = 0;
		while (current != null && indexTracker != index) {
			previous = current;
			current = current.next;
			indexTracker++;
		}
		Node newNode = new Node(data);

		if (previous != null) {
			previous.next = newNode;
			newNode.next = current;
		} else {
			previous = newNode;
			previous.next = current;
			head = previous;
		}
		size++;
	}

	private void print(Node head) {
		if (head == null) {
			System.out.println("Empty!!!");
			return;
		} else {
			Node temp = head;
			while (temp != null) {
				System.out.println(temp.data);
				temp = temp.next;
			}
		}
	}
}